import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator'
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { UsuarioDataI } from 'src/app/interfaces/usuariodata1';
import { UsuarioService } from 'src/app/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})


export class UsuariosComponent implements OnInit, AfterViewInit {
 listUsuarios: UsuarioDataI[] = [];

  displayedColumns: string[] = ['usuario', 'nombre', 'apellido', 'sexo', 'acciones'];
  dataSource! :MatTableDataSource<any>;

@ViewChild(MatPaginator) paginator!: MatPaginator;

@ViewChild(MatSort) sort!: MatSort;

  constructor(private _usuarioService: UsuarioService,
              private _snackbar: MatSnackBar,
              private router: Router) { }

  ngOnInit(): void {
    this.cargarUsuario();
  }


  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


  cargarUsuario(){
    this.listUsuarios = this._usuarioService.getUsuario();
    this.dataSource = new MatTableDataSource(this.listUsuarios); 
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
      
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarUsuario(usuario: string){
    const opcion = confirm('Estas seguro de eliminar el usuario');

    if(opcion){
    console.log(usuario);
    this._usuarioService.eliminarUsuario(usuario);
    this.cargarUsuario();

    this._snackbar.open('El usuario fue eliminado con exito', '',{
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom'
    });
   }
  }

  verUsuario(usuario: string){
    this.router.navigate(['dashboard/ver-usuario',usuario]);
  }

  modificarUsuario(usuario: string){
    console.log(usuario);
    this.router.navigate(['dashboard/crear-usuario', usuario]);
  }
    

}
