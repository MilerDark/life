import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioDataI } from 'src/app/interfaces/usuariodata1';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  tiles: any[] = [
      {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
      {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
      {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
      {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'},
  ];
  sexo: any[] = ['Masculino', 'Femenino'];

  adicionar = true;
  titulo = 'Crear Usuario';
  form!: FormGroup;
  constructor(private fb: FormBuilder,
              private _usuarioService: UsuarioService,
              private router: Router,
              private _snackbar: MatSnackBar,
              private activateRouted: ActivatedRoute) {

    this.activateRouted.params.subscribe(params => {
      const id = params['id'];
      console.log(id);

      this.form = this.fb.group({
        usuario: ['',Validators.required],
        nombre: ['',Validators.required],
        apellido: ['',Validators.required],
        sexo: ['',Validators.required]
      });
      if(id !== 'nuevo'){
        const usuario = this._usuarioService.buscarUsuario(id);
        console.log(usuario);
        //verifico que la longitud del objeto es cero
        
        if(Object.keys(usuario).length === 0){
          this.router.navigate(['/dashboard/usuarios']);
        }

        this.form.patchValue({
          usuario: usuario.usuario,
          nombre: usuario.nombre,
          apellido: usuario.apellido,
          sexo: usuario.sexo
        });
        this.adicionar = false;
        this.titulo = 'Modificar Usuario';
        
      }
      
    });
    //this.insertarUsuario();
  }
      
  ngOnInit(): void {
  }

  insertarUsuario():void{
    this.form = this.fb.group({
      usuario: ['',Validators.required],
      nombre: ['',Validators.required],
      apellido: ['',Validators.required],
      sexo: ['',Validators.required]
    });

  }

  agregarUsuario(): void {
   if(!this.form.valid){
   return;
  }
    const user: UsuarioDataI = {
    usuario: this.form.value.usuario,
    nombre: this.form.value.nombre,
    apellido: this.form.value.apellido,
    sexo: this.form.value.sexo,
  }
  if(this.adicionar){
    this._usuarioService.agregarUsuario(user);
    this.router.navigate(['/dashboard/usuarios']);
    this._snackbar.open('El usuario fue agregado con exito', '', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom'
    });
  } else{
      this._usuarioService.modificarUsuario(user);
      this.router.navigate(['/dashboard/usuarios']);
      this._snackbar.open('El usuario fue modificado con exito', '',{
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });
    }
  
  
    
  }
  Volver(): void {
    this.router.navigate(['/dashboard/usuarios']);
   }
    
 }
  
   